// See http://code.google.com/p/googletest/wiki/Primer#Writing_the_main%28%29_Function
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include "gtest/gtest.h"
#include "ExampleCMake.hpp"
using namespace std;

namespace {

// The fixture for testing class Foo.
class TestExampleCMake : public ::testing::Test {
 protected:
  // You can remove any or all of the following functions if its body
  // is empty.


  ExampleCMake myObject;

  TestExampleCMake() {
    // You can do set-up work for each test here.
    myObject.set_values(4,4);
  }

  virtual ~TestExampleCMake() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  virtual void SetUp() {
    // Code here will be called immediately after the constructor (right
    // before each test).
  }

  virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
  }

  // Objects declared here can be used by all tests in the test case for Foo.
};


TEST_F(TestExampleCMake, TestOne) {

   EXPECT_EQ(myObject.area(), 16);
}

TEST_F(TestExampleCMake, TestTwo) {
   EXPECT_NE(myObject.area(), 15);
}




}


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
