# ExampleCMake
 
An example skeleton for a C++ project with CMake and GoogleTest.

# Quick Start

This will compile and run the unit tests for ExampleCMake

## [Ubuntu Precise](http://releases.ubuntu.com/precise/)

    # Ensure you have what is needed to build the code
    $ sudo apt-get install git cmake g++

    # Clone the repository
    $ git clone https://bitbucket.org/mjw99/ExampleCMake.git

    # Create directories for the out-of-source build
    $ mkdir ExampleCMakeBuild ; cd ExampleCMakeBuild

    # Build, compile and test
    $ cmake ../ExampleCMake ; make ; make test
