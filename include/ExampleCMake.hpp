#ifndef EXAMPLECMAKE_H_
#define EXAMPLECMAKE_H_

class ExampleCMake {
    int x, y;
  public:
    void set_values (int,int);
    int area () {return (x*y);}

    ExampleCMake();
    virtual ~ExampleCMake();
};

#endif /* EXAMPLECMAKE_H_ */
